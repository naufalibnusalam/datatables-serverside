const progressFill = document.querySelectorAll(".progress-fill");
const progressNumber = document.querySelectorAll(".progress-number");

const resultCalculate = () => {
    for (let i = 0; i < progressFill.length; i++) {
        const start = progressFill[i].getAttribute("data-start");
        const end = progressFill[i].getAttribute("data-end")
        progressFill[i].style.width = start / end * 100 + "%";
        progressNumber[i].style.left = (start / end * 100) - 7 + "%";

        if (progressNumber[i].style.left > "85%") {
            progressNumber[i].style.left = 85 + "%";
        }
    }
}

window.addEventListener("load", () => {
    resultCalculate();
});


/* POP LOGIN MODAL */

const popLogModal = () => {
    const loginModal = document.querySelector("#login-modal");
    const loginWrapper = document.querySelector("#form-wrapper-log");
    const container = document.querySelector("#container-log");
    const loginBtn = document.querySelector("#login-btn");
    const closeIcon = document.querySelector("#close-icon-log");

    const body = document.body;

    if (loginBtn) {
        loginBtn.addEventListener("click", () => {
            loginBtn.disabled = true;
            loginModal.style.display = "flex";
            setTimeout(() => {
                loginModal.classList.add("show");
            }, 100);
            setTimeout(() => {
                loginWrapper.classList.add("show");
            }, 200);
            setTimeout(() => {
                body.classList.add("hide-scroll");
            }, 300);
        });

        loginModal.addEventListener("click", (e) => {
            if (e.target == loginModal || e.target == container) {
                loginBtn.disabled = false;
                body.classList.remove("hide-scroll");
                loginModal.classList.remove("show");
                loginWrapper.classList.remove("show");
                setTimeout(() => {
                    loginModal.style.display = "none";
                }, 200);
            }
        });

        closeIcon.addEventListener("click", () => {
            loginBtn.disabled = false;
            body.classList.remove("hide-scroll");
            loginModal.classList.remove("show");
            loginWrapper.classList.remove("show");
            setTimeout(() => {
                loginModal.style.display = "none";
            }, 200);
        });
    }
}

popLogModal();

/* POP REGISTER MODAL */

const popRegModal = () => {
    const registerModal = document.querySelector("#register-modal");
    const registerWrapper = document.querySelector("#form-wrapper-reg");
    const container = document.querySelector("#container-reg");
    const registerBtn = document.querySelector("#register-btn");
    const closeIcon = document.querySelector("#close-icon-reg");

    const body = document.body;

    if (registerBtn) {
        registerBtn.addEventListener("click", () => {
            registerBtn.disabled = true;
            registerModal.style.display = "flex";
            setTimeout(() => {
                registerModal.classList.add("show");
            }, 100);
            setTimeout(() => {
                registerWrapper.classList.add("show");
            }, 200);
            setTimeout(() => {
                body.classList.add("hide-scroll");
            }, 300);
        });

        registerModal.addEventListener("click", (e) => {
            if (e.target == registerModal || e.target == container) {
                registerBtn.disabled = false;
                body.classList.remove("hide-scroll");
                registerModal.classList.remove("show");
                registerWrapper.classList.remove("show");
                setTimeout(() => {
                    registerModal.style.display = "none";
                }, 200);
            }
        });

        closeIcon.addEventListener("click", () => {
            registerBtn.disabled = false;
            body.classList.remove("hide-scroll");
            registerModal.classList.remove("show");
            registerWrapper.classList.remove("show");
            setTimeout(() => {
                registerModal.style.display = "none";
            }, 200);
        });
    }
}

popRegModal();

/* POP LOGIN MODAL IF THERE'S ANY ERROR(S) */

const popLogError = () => {
    const loginModal = document.querySelector("#login-modal");
    const body = document.body;

    if (loginModal) {
        const errMessage = loginModal.querySelectorAll(".invalid-feedback");
        if (errMessage && errMessage.length > 0) {
            loginModal.style.display = "flex";
            loginModal.style.opacity = 1;
            body.classList.add("hide-scroll");

            return true;
        }
    }
    return false;
}

popLogError();

/* POP REGISTER MODAL IF THERE'S ANY ERROR(S) */

const popRegError = () => {
    const registerModal = document.querySelector("#register-modal");
    const body = document.body;

    if (registerModal) {
        const errMessage = registerModal.querySelectorAll(".invalid-feedback");
        if (errMessage && errMessage.length > 0) {
            if (registerModal) {
                registerModal.style.display = "flex";
                registerModal.style.opacity = 1;
            }
            body.classList.add("hide-scroll");

            return true;
        }
    }
    return false;
}

popRegError();

/* ENABLE SAVE BUTTON IN PROFIL USER PAGE OR DISABLE IT */

const enableSave = () => {
    const profileForm = document.getElementById("profile-form");
    const saveProfile = document.getElementById("save-profile");

    window.addEventListener("load", () => {
        if (saveProfile) {
            saveProfile.disabled = true;
        }
    });


    if (profileForm) {
        profileForm.addEventListener("keyup", () => {
            saveProfile.disabled = false;
        });
    }

    if (profileForm) {
        profileForm.addEventListener("change", () => {
            saveProfile.disabled = false;
        });
    }
}

enableSave();

/* REPLACE NON DIGIT IN NOMOR TELEPON FIELD */

const replaceNonDigits = () => {
    const telepon = document.getElementById("telepon");

    const validDigit = (n) => {
        return n.replace(/[^0-9+]+/g, '');
    }

    if (telepon) {
        telepon.addEventListener("keyup", () => {
            var fieldValue = telepon.value;
            telepon.value = validDigit(fieldValue);
        });
    }
}

replaceNonDigits();

/* CHANGE COLOR IN SELECT BOX IF THE VALUE WAS "" */


const changeColorSelect = () => {
    const profileForm = document.getElementById("profile-form");

    if (profileForm) {
        const selectBoxes = profileForm.querySelectorAll("select");

        for (let i = 0; i < selectBoxes.length; i++) {
            selectBoxes[i].addEventListener("change", () => {
                if (selectBoxes[i].value === "") {
                    selectBoxes[i].style.color = "#8898aa";
                } else {
                    selectBoxes[i].style.color = "#32325d";
                }
            });
        }
    }
}

window.addEventListener("load", () => {
    const profileForm = document.getElementById("profile-form");

    if (profileForm) {
        const selectBoxes = profileForm.querySelectorAll("select");

        for (let i = 0; i < selectBoxes.length; i++) {
            if (selectBoxes[i].value === "") {
                selectBoxes[i].style.color = "#8898aa";
            } else {
                selectBoxes[i].style.color = "#32325d";
            }
        }
    }
    changeColorSelect();
});

/* MUTE COLOR TEXT INSIDE INPUT DATE WHEN PAGE IS ACTUALLY LOADDED */

const changeColorDate = () => {
    const profileForm = document.getElementById("profile-form");

    if (profileForm) {
        const inputsDate = profileForm.querySelectorAll('input[type="date"]')

        for (let i = 0; i < inputsDate.length; i++) {
            inputsDate[i].addEventListener("keyup", () => {
                if (inputsDate[i].value === "") {
                    inputsDate[i].style.color = "#8898aa";
                } else {
                    inputsDate[i].style.color = "#32325d";
                }
            });
        }

    }
}

window.addEventListener("load", () => {
    const profileForm = document.getElementById("profile-form");

    if (profileForm) {
        const inputsDate = profileForm.querySelectorAll('input[type="date"]')

        for (let i = 0; i < inputsDate.length; i++) {
            if (inputsDate[i].value === "") {
                inputsDate[i].style.color = "#8898aa";
            } else {
                inputsDate[i].style.color = "#32325d";
            }
        }
    }
    changeColorDate();
});

/* COURSE CONTROL INDEX AND CHANGE BG DEPENDS ON WHAT COURSE SHOWED */

const classLists = () => {
    const daftarKelas = document.querySelector("#daftar-kelas");
    const courseLists = document.querySelector(".course-lists");
    const backButton = document.querySelector(".back-button");
    const nextButton = document.querySelector(".next-button");
    let index = 0;

    const backgrounds = ["it-bg", "sosbud-bg", "agama-bg", "donasi-bg"];

    function showClass(index) {
        if (courseLists) {
            const courseItems = courseLists.querySelectorAll(".course-items");
            for (let i = 0; i < courseItems.length; i++) {
                courseItems[i].style.display = "block";
                courseItems[i].style.maxHeight = 100 + "vh";
                courseItems[i].style.opacity = 0;
            }
            courseItems[index].style.maxHeight = "none";
            courseItems[index].style.opacity = 1;

            for (let i = 0; i < backgrounds.length; i++) {
                if (courseItems[index].classList.contains("it-class")) {
                    daftarKelas.classList.remove(backgrounds[i]);
                    daftarKelas.classList.add("it-bg");
                } else if (courseItems[index].classList.contains("sosbud-class")) {
                    daftarKelas.classList.remove(backgrounds[i]);
                    daftarKelas.classList.add("sosbud-bg");
                } else if (courseItems[index].classList.contains("agama-class")) {
                    daftarKelas.classList.remove(backgrounds[i]);
                    daftarKelas.classList.add("agama-bg");
                } else {
                    daftarKelas.classList.remove(backgrounds[i]);
                    daftarKelas.classList.add("donasi-bg");
                }
            }
        }
    }

    function minusIndex() {
        showClass(index -= 1);
    }

    function plusIndex() {
        showClass(index += 1);
    }

    showClass(index);

    if (backButton) {
        backButton.addEventListener("click", () => {
            const courseItems = courseLists.querySelectorAll(".course-items");
            courseLists.style.marginLeft = -(index - 1) * 100 + "%";
            nextButton.style.display = "block";
            if (index > 0) {
                minusIndex();
                backButton.style.display = "block";
            }
            if (index < 1) {
                backButton.style.display = "none";
            }
        });
    }

    if (nextButton) {
        nextButton.addEventListener("click", () => {
            const courseItems = courseLists.querySelectorAll(".course-items");
            courseLists.style.marginLeft = -(index + 1) * 100 + "%";
            backButton.style.display = "block";
            if (index < (courseItems.length - 1)) {
                plusIndex();
                nextButton.style.display = "block";
            }
            if (index > (courseItems.length - 2)) {
                nextButton.style.display = "none";
            }
        });
    }

    window.addEventListener("load", () => {
        if (courseLists) {
            const courseItems = courseLists.querySelectorAll(".course-items");
            backButton.style.display = "none";
            if (courseItems.length > 1) {
                nextButton.style.display = "block";
            }

            courseLists.style.width = courseItems.length * 100 + "%";
            for (let i = 0; i < courseItems.length; i++) {
                courseItems[i].style.width = 100 / courseItems.length + "%";
            }
        }
    });
}

classLists();

/* MAP DETAIL TOGGLE MENU */

//Accordian Action
var action = 'click';
var speed = "500";

//Document.Ready
$(document).ready(function () {
    //Question handler
    $('.faq-title').on(action, function () {
        //gets next element
        //opens .a of selected question
        $(this).toggleClass('button-active').next().slideToggle(speed)
            //selects all other answers and slides up any open answer
            .siblings('.faq-answer').slideUp();
        // Kalo salah satu button udah diberikan class active, remove class active 
        // kecuali button yang ditarget
        var remove = $('.faq-title').not(this);
        remove.removeClass('button-active');

        //Grab img from clicked question
        var img = $(this).find('span');
        //Remove Rotate class from all images except the active
        $('span').not(img).removeClass('rotate');
        //toggle rotate class
        img.toggleClass('rotate');
    });//End on click
});//End Ready