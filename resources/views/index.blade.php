<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <!-- CSS -->
    <!-- css main -->
    <link rel="stylesheet" href="{{ asset('assets/main_component/css/main.css') }}">
    <!-- css dashboard -->
    <link rel="stylesheet" href="{{ asset('assets/main_component/css/dashboard.css') }}">
    <!-- css argon -->
    <link rel="stylesheet" href="{{ asset('assets/second_component/css/argon-dashboard.css') }}">
    <!-- css datatables -->
    <link rel="stylesheet" href="{{ asset('assets/main_component/css/dataTables.bootstrap4.min.css') }}">

    <!-- JS -->
    <!-- js argon -->
    <script defer src="{{ asset('assets/second_component/js/argon-dashboard.min.js') }}"></script>
    <!-- js jquery -->
    <script src="{{ asset('assets/second_component/js/jquery.min.js') }}"></script>
    <!-- js datatables -->
    <script src="{{ asset('assets/main_component/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/main_component/js/dataTables.bootstrap4.min.js') }}"></script>

</head>

<body>
    <div id="app">
        <table id="table" class="align-items-center table table-bordered">
            <thead class="table-light">
                <tr>
                    <td><span class="mb-0 h5">No</span></td>
                    <td><span class="mb-0 h5">NIS</span></td>
                    <td><span class="mb-0 h5">Nama</span></td>
                    <td><span class="mb-0 h5">Kelas</span></td>
                    <td><span class="mb-0 h5">Alamat</span></td>
                    <td><span class="mb-0 h5">HP</span></td>
                </tr>
            </thead>
        </table>
    </div>
    <script>
        $(document).ready(function () {
            $('#table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": '{!! route('datatables') !!}',
                "columns": [
                    { "data": "no" },
                    { "data": "nis" },
                    { "data": "nama" },
                    { "data": "kelas" },
                    { "data": "alamat" },
                    { "data": "hp" }
                ]	     
            });
        });
    </script>

</body>

</html>