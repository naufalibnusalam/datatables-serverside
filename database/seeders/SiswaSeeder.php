<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

class SiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');
        for($i = 1; $i <= 5689; $i++){
            // insert data ke table siswa menggunakan Faker
            \DB::table('tb_siswa')->insert([
                'nis' => $faker->randomDigit,
                'nama' => $faker->name,
                'kelas' => $faker->company,
                'alamat' => $faker->address,
                'hp' => $faker->numberBetween(8000,9000)
            ]);
        }
    }
}
