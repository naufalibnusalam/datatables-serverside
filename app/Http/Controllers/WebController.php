<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class WebController extends Controller
{
    public function index() {
        return view('index')->with(['title' => 'Datatables Serverside']);
    }

    public function datatables(Request $request){
        $columns = array(
            0 => 'id',
            1 => 'nis',
            2 => 'nama',
            3 => 'kelas',
            4 => 'alamat',
            5 => 'hp'
        );
        $totalData = DB::table('tb_siswa')->count();
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');            
        if(empty($request->input('search.value'))){            
            $siswa = DB::table('tb_siswa')
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();
        }else{
            $search = $request->input('search.value');
            $siswa =  DB::table('tb_siswa')
                        ->where('nis','LIKE',"%{$search}%")
                        ->orWhere('nama','LIKE',"%{$search}%")
                        ->orWhere('kelas','LIKE',"%{$search}%")
                        ->orWhere('alamat','LIKE',"%{$search}%")
                        ->orWhere('hp','LIKE',"%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
            $totalFiltered = DB::table('tb_siswa')
                                ->where('nis','LIKE',"%{$search}%")
                                ->orWhere('nama','LIKE',"%{$search}%")
                                ->orWhere('kelas','LIKE',"%{$search}%")
                                ->orWhere('alamat','LIKE',"%{$search}%")
                                ->orWhere('hp','LIKE',"%{$search}%")
                                ->count();
        }
        $data = array();
        if(!empty($siswa)){
            $no = 1;
            foreach ($siswa as $row){
                $nestedData['no'] = $no++;
                $nestedData['nis'] = $row->nis;
                $nestedData['nama'] = $row->nama;
                $nestedData['kelas'] = $row->kelas;
                $nestedData['alamat'] = $row->alamat;
                $nestedData['hp'] = $row->hp;
                $data[] = $nestedData;
            }
        }
        $json_data = array(
                        "draw"            => intval($request->input('draw')),  
                        "recordsTotal"    => intval($totalData),  
                        "recordsFiltered" => intval($totalFiltered), 
                        "data"            => $data   
                    );
        echo json_encode($json_data);
    }
}
